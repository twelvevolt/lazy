allAvg :: [Int] -> [Int]
allAvg l = l'
  where
    (l', a) = let a'       = round $ fromIntegral rs / (fromIntegral $ length l)
                  (rl, rs) = go l
              in (rl, a')

    go []     = ([], 0)
    go (x:xs) = let s        = x + rs
                    (rl, rs) = go xs
                    nl       = a : rl
                in (nl, s)
